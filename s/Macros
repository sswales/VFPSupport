; 
; Copyright (c) 2010, RISC OS Open Ltd
; All rights reserved.
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met: 
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
; 
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
; 

; Macros to make up for the fact that objasm doesn't support the required instructions yet

; Instruction Synchronisation Barrier - required on ARMv6+ to ensure the effects of the following are visible to following instructions:
; * Completed cache, TLB & branch predictor maintenance operations
; * CP14/CP15 writes
; This version will only work on ARMv6+!
        MACRO
        myISB $cond,$temp
       [ NoARMv7
        ; ARMv6 support required, use legacy MCR op
        MOV$cond $temp,#0
        MCR$cond p15,0,$temp,c7,c5,4
       |
        ; ARMv7+, use ISB instruction (saves on temp register, but instruction is unconditional)
        ; Shouldn't hurt too much if we just ignore the condition code
        ISB     SY
       ]
        MEND

; VMRS
        MACRO
        myVMRS $cond,$reg,$fpreg
      [ "$fpreg" = "FPSID"
        MRC$cond p10,7,$reg,c0,c0,0
      ELIF "$fpreg" = "FPSCR"
        MRC$cond p10,7,$reg,c1,c0,0
      ELIF "$fpreg" = "MVFR2"
        MRC$cond p10,7,$reg,c5,c0,0
      ELIF "$fpreg" = "MVFR1"
        MRC$cond p10,7,$reg,c6,c0,0
      ELIF "$fpreg" = "MVFR0"
        MRC$cond p10,7,$reg,c7,c0,0
      ELIF "$fpreg" = "FPEXC"
        MRC$cond p10,7,$reg,c8,c0,0
      ELIF "$fpreg" = "FPINST"
        MRC$cond p10,7,$reg,c9,c0,0
      |
        ASSERT "$fpreg" = "FPINST2"
        MRC$cond p10,7,$reg,c10,c0,0
      ]
        MEND

; VMSR
        MACRO
        myVMSR $cond,$fpreg,$reg
      [ "$fpreg" = "FPSID"
        MCR$cond p10,7,$reg,c0,c0,0
      ELIF "$fpreg" = "FPSCR"
        MCR$cond p10,7,$reg,c1,c0,0
      ELIF "$fpreg" = "FPEXC"
        MCR$cond p10,7,$reg,c8,c0,0
      ELIF "$fpreg" = "FPINST"
        MCR$cond p10,7,$reg,c9,c0,0
      |
        ASSERT "$fpreg" = "FPINST2"
        MCR$cond p10,7,$reg,c10,c0,0
      ]
        MEND

; Maths macros for manipulating floating point values at the bit field
; level within VFPSupport

FLT_MANT_DIG    * 24
FLT_EXP_BIAS    * 127
DBL_MANT_DIG    * 53
DBL_EXP_BIAS    * 1023

        MACRO
        ExpBits64 $exp, $hiword
      [ NoARMT2
        MOV     $exp, $hiword, LSL #1
        MOV     $exp, $exp, LSR #21
      |
        UBFX    $exp, $hiword, #20, #11
      ]
        MEND

        MACRO
        ExpBits32 $exp, $word
      [ NoARMT2
        MOV     $exp, $word, LSL #1
        MOV     $exp, $exp, LSR #24
      |
        UBFX    $exp, $word, #23, #8
      ]
        MEND

        MACRO
        LoadExp64 $reg, $offset
        LDR     $reg, =(DBL_EXP_BIAS + $offset):SHL:20
        MEND

        MACRO
        LoadExp32 $reg, $offset
        LDR     $reg, =(FLT_EXP_BIAS + $offset):SHL:23
        MEND

        MACRO
        VFused64 $addend, $x, $y
    [ NoARMVv4
        LCLA    maxreg
maxreg  SETA    7                       ; Always d0-d7
      [ $x > &$maxreg
maxreg  SETA    $x
      ]
      [ $y > &$maxreg
maxreg  SETA    $y
      ]
      [ $addend > &$maxreg
maxreg  SETA    $addend
      ]
        DCI     &ED2D0B00 + ((maxreg + 1) * 2) ; VPUSH d0...maxreg
      [ $x <> 0
        VLDR    d0, [sp, #$x * 8]
      ]
      [ $y <> 1
        VLDR    d1, [sp, #$y * 8]
      ]
      [ $addend <> 2
        VLDR    d2, [sp, #$addend * 8]
      ]
        Push    "a1-a4, ip, lr"
        BL      fused64_muladd
        Pull    "a1-a4, ip, lr"
      [ $addend <> 0
        VSTR    d0, [sp, #$addend * 8]
        DCI     &ECBD0B00 + ((maxreg + 1) * 2) ; VPOP d0...maxreg
      |
        ADD     sp, sp, #8
        DCI     &ECBD1B00 + ((maxreg + 0) * 2) ; VPOP d1...maxreg
      ]
    |
        VFMA.F64 $addend, $x, $y
    ]
        MEND

        MACRO
        VFused32 $addend, $x, $y
    [ NoARMVv4
        LCLA    maxreg
maxreg  SETA    15                      ; Always s0-s15
      [ $x > &$maxreg
maxreg  SETA    $x
      ]
      [ $y > &$maxreg
maxreg  SETA    $y
      ]
      [ $addend > &$maxreg
maxreg  SETA    $addend
      ]
        DCI     &ED2D0A00 + (maxreg + 1) ; VPUSH s0...maxreg
      [ $x <> 0
        VLDR    s0, [sp, #$x * 4]
      ]
      [ $y <> 1
        VLDR    s1, [sp, #$y * 4]
      ]
      [ $addend <> 2
        VLDR    s2, [sp, #$addend * 4]
      ]
        Push    "a1-a4, ip, lr"
        BL      fused32_muladd
        Pull    "a1-a4, ip, lr"
      [ $addend <> 0
        VSTR    s0, [sp, #$addend * 4]
        DCI     &ECBD0A00 + (maxreg + 1) ; VPOP s0...maxreg
      |
        ADD     sp, sp, #4
        DCI     &ECFD0A00 + (maxreg + 0) ; VPOP s1...maxreg
      ]
    |
        VFMA.F32 $addend, $x, $y
    ]
        MEND

        MACRO
        IsNaNorINF64 $dreg, $temp
        LCLA    hiword
        LCLS    sreg
hiword  SETA    ($dreg * 2) + 1         ; High word of D<n> is S<2n+1>
      [ hiword > 10
sreg    SETS    (:STR:(hiword/10)):RIGHT:1
      ]
sreg    SETS    "S$sreg" :CC:((:STR:(hiword%10)):RIGHT:1)
        VMOV    $temp, $sreg
      [ NoARMT2
        MOV     $temp, $temp, LSL #1
        MOV     $temp, $temp, ASR #21
      |
        SBFX    $temp, $temp, #20, #11
      ]
        CMP     $temp, #-1              ; Exponent &7FF
        MEND

        MACRO
        IsNaNorINF32 $sreg, $temp
        VMOV    $temp, $sreg
      [ NoARMT2
        MOV     $temp, $temp, LSL #1
        MOV     $temp, $temp, ASR #24
      |
        SBFX    $temp, $temp, #23, #8
      ]
        CMP     $temp, #-1              ; Exponent &FF
        MEND

        MACRO
        IsZeroOrSubNormal64 $dreg, $temp
        LCLA    hiword
        LCLS    sreg
hiword  SETA    ($dreg * 2) + 1         ; High word of D<n> is S<2n+1>
      [ hiword > 10
sreg    SETS    (:STR:(hiword/10)):RIGHT:1
      ]
sreg    SETS    "S$sreg" :CC:((:STR:(hiword%10)):RIGHT:1)
        VMOV    $temp, $sreg
        MOV     $temp, $temp, LSL #1
        MOVS    $temp, $temp, LSR #21   ; Exponent &000
        MEND

        MACRO
        IsZeroOrSubNormal32 $sreg, $temp
        VMOV    $temp, $sreg
        MOV     $temp, $temp, LSL #1
        MOVS    $temp, $temp, LSR #24   ; Exponent &00
        MEND

        END
